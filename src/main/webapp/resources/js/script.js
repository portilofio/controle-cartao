(function (win) {
	"use strict";
	const gerenciadorMensagem = {
		exibirMensagem: function(msg) {
			win.toastr.options.closeButton = true;
			const msgSplit = msg.split(':');
			switch(msgSplit[0]) {
				case 'INFO':
					win.toastr.success(msgSplit[1]);
				break;
				
				case 'ERROR':
					win.toastr.error(msgSplit[1]);
				break;
			}
		}
	}
	
	win.gerenciadorMensagem = gerenciadorMensagem;
})(window);