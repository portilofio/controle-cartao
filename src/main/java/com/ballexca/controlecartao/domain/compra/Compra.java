package com.ballexca.controlecartao.domain.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;

@Entity
public class Compra implements Serializable {

	public enum Tipo {
		CREDITO("Crédito"), DEBITO("Débito");
		
		private String label;
		
		private Tipo(String label) {
			this.label = label;
		}
	
		public String getLabel() {
			return this.label;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(nullable = false)
	private CartaoCredito cartaoCredito;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date data;
	
	@Column(nullable = false)
	private Byte numParcelas;
	
	@Column(nullable = false)
	private BigDecimal valor;
	
	@Column(nullable = false)
	private String descricao;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Tipo tipo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Byte getNumParcelas() {
		return numParcelas;
	}

	public void setNumParcelas(Byte numParcelas) {
		this.numParcelas = numParcelas;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compra other = (Compra) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Compra [getId()=");
		builder.append(getId());
		builder.append(", getCartaoCredito()=");
		builder.append(getCartaoCredito());
		builder.append(", getData()=");
		builder.append(getData());
		builder.append(", getNumParcelas()=");
		builder.append(getNumParcelas());
		builder.append(", getDescricao()=");
		builder.append(getDescricao());
		builder.append(", getTipo()=");
		builder.append(getTipo());
		builder.append(", getValor()=");
		builder.append(getValor());
		builder.append("]");
		return builder.toString();
	}
}
