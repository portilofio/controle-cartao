package com.ballexca.controlecartao.domain.compra;

import java.util.List;

import javax.inject.Inject;

import com.ballexca.controlecartao.adapters.db.LancamentoJpaRepository;
import com.ballexca.controlecartao.domain.Service;
import com.ballexca.controlecartao.domain.exception.NegocioException;
import com.ballexca.controlecartao.domain.lancamento.Lancamento;
import com.ballexca.controlecartao.domain.lancamento.LancamentoService;
import com.ballexca.controlecartao.domain.ports.CompraRepository;

public class CompraServiceImpl extends Service {

	private LancamentoJpaRepository lancamentoDAO;

	private CompraRepository compraRepository;
	
	private LancamentoService lancamentoService;
	
	public CompraServiceImpl() {
		
	}
	
	@Inject
	public CompraServiceImpl(
			CompraRepository compraRepository,
			LancamentoService lancamentoService) {
		
		this.compraRepository = compraRepository;
		this.lancamentoService = lancamentoService;
		
	}
	
	public void salvar(Compra compra) {
		try {
			beginTransaction();

			compraRepository.save(compra);

			lancamentoService.gerarLancamentos(compra);
			
			commitTransaction();
		} catch (RuntimeException e) {
			rollbackTransaction();
			throw new NegocioException(e);
		}
	}
	
	/**
	 * Excluí uma compra e todos os lançamentos associados a ela.
	 */
	public void excluirCompra(Compra compra) {
		try {
			beginTransaction();
		
			// Obtêm a lista de lançamentos da compra.
			List<Lancamento> lancamentos = lancamentoDAO.listarLancamentosCompra(compra.getId());
			
			// Exclui cada um dos lançamentos.
			for (Lancamento lancamento : lancamentos) {
				lancamento.setCompra(null);
				// lancamentoDAO.excluir(lancamento);
			}
			
			compraRepository.delete(compra);
					
			commitTransaction();
		} catch (RuntimeException e) {
			rollbackTransaction();
			throw e;
		}
	}
}
