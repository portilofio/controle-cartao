package com.ballexca.controlecartao.domain.exception;

public class CartaoExistenteException extends RuntimeException {

	private static final String MSG_CARTAO_EXISTENTE = "Esse número de cartão de crédito já está cadastrado!";
	
	public CartaoExistenteException() {
		super(MSG_CARTAO_EXISTENTE);
	}
}
