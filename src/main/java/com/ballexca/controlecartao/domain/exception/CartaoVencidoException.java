package com.ballexca.controlecartao.domain.exception;

public class CartaoVencidoException extends RuntimeException {

	private static final String MSG_CARTAO_VENCIDO = "Cartão de crédito vencido!";
	
	public CartaoVencidoException() {
		super(MSG_CARTAO_VENCIDO);
	}
}
