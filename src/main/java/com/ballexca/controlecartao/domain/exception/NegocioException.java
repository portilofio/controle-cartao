package com.ballexca.controlecartao.domain.exception;

/**
 * Exceção lançada quando ocorre uma exception na camada de negócio.
 */
public class NegocioException extends RuntimeException {

	public static final String MSG_NUMERO_CARTAO_ALTERADO 
	= "Não é permitido alterar o número de um cartão de crédito cadastrado.";
	
	public NegocioException() {
	}

	public NegocioException(String message) {
		super(message);
	}

	public NegocioException(Throwable cause) {
		super(cause);
	}

	public NegocioException(String message, Throwable cause) {
		super(message, cause);
	}
}
