package com.ballexca.controlecartao.domain.ports;

import java.util.Optional;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;

public interface CartaoCreditoRepository extends CrudRepository<Integer, CartaoCredito> {
	
	Optional<CartaoCredito> findByNumero(String numero);
	
	Optional<CartaoCredito> findByNumeroAndId(String numero, Integer id);
	
	long countByNumero(String numero);
	
	long countById(Integer numero);
}
