package com.ballexca.controlecartao.domain.ports;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<PK, E> {

	List<E> findAll();

	Optional<E> findById(PK id);

	E save(E entity);

	E update(E entity);

	void delete(E entity);

	void deleteById(PK primaryKey);
}
