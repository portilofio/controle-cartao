package com.ballexca.controlecartao.domain.ports;

import java.util.List;
import java.util.Optional;

import com.ballexca.controlecartao.domain.lancamento.Lancamento;

public interface LancamentoRepository extends CrudRepository<Integer, Lancamento> {

	Optional<Lancamento> findByCompraIdAndNumeroParcela(Integer compraId, Integer numeroParcela);
	
	
	/*Serao refatorados*/
	List<Lancamento> pesquisarLancamentos(Integer mes, Integer ano, Integer cartaoCreditoId);
	List<Lancamento> listarLancamentosCompra(Integer compraId);
	List<Lancamento> listarLancamentosFatura(Integer faturaId);
	double somarLancamentosAbertos(Integer cartaoCreditoId);
}
