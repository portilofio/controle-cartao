package com.ballexca.controlecartao.domain.ports;

import com.ballexca.controlecartao.domain.compra.Compra;

public interface CompraRepository extends CrudRepository<Integer, Compra> {

}
