package com.ballexca.controlecartao.domain.fatura;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.ballexca.controlecartao.adapters.db.CrudJPA;
import com.ballexca.controlecartao.domain.util.DateUtils;

/**
 * Classe CrudJPA da entidade Fatura
 */
public class FaturaDAO extends CrudJPA {
	
	/**
	 * Obt�m uma lista de faturas cadastradas para um cart�o
	 */
	@SuppressWarnings("unchecked")
	public List<Fatura> listarFaturas(Integer cartaoCreditoId) {
		Query q = criarQuery("SELECT f FROM Fatura f ORDER BY f.dataVencimento DESC");
		q.setMaxResults(5);
		return q.getResultList();
	}
	
	/**
	 * Verifica se j� existe uma fatura cadastrada para o cart�o em determinado m�s/ano
	 */
	public boolean existeFatura(Integer cartaoCreditoId, Integer mes, Integer ano) {
		
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.set(ano, mes, 1);
		
		Calendar dataFim = Calendar.getInstance();
		dataFim.set(ano, mes, DateUtils.getMaxDays(mes, ano));
		
		Query q = criarQuery("SELECT COUNT(f) FROM Fatura f WHERE f.cartaoCredito.id = " + cartaoCreditoId + " AND f.dataVencimento >= :dataInicio AND f.dataVencimento <= :dataFim ");
		q.setParameter("dataInicio", dataInicio, TemporalType.DATE);
		q.setParameter("dataFim", dataFim, TemporalType.DATE);
		
		long count = (Long) q.getResultList().get(0);
		return count > 0;
	}
}
