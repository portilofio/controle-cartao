package com.ballexca.controlecartao.domain.lancamento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.ballexca.controlecartao.domain.Service;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCreditoService;
import com.ballexca.controlecartao.domain.compra.Compra;
import com.ballexca.controlecartao.domain.compra.Compra.Tipo;
import com.ballexca.controlecartao.domain.ports.LancamentoRepository;
import com.ballexca.controlecartao.domain.util.DateUtils;

public class LancamentoService extends Service {

  private CartaoCreditoService cartaoCreditoService;
  private LancamentoRepository lancamentoRepository;

  @Inject
  public LancamentoService(LancamentoRepository lancamentoRepository, CartaoCreditoService cartaoCreditoService) {

    this.lancamentoRepository = lancamentoRepository;
    this.cartaoCreditoService = cartaoCreditoService;
  }

  public LancamentoService() {
  }

  public List<Lancamento> gerarLancamentos(Compra compra) {

    byte numParcelas = compra.getNumParcelas();
    BigDecimal valorTotal = compra.getValor();

    double valorParcela = valorTotal.divide(new BigDecimal((int) numParcelas)).doubleValue();

    Optional<CartaoCredito> cartaoCreditoOp = cartaoCreditoService
        .pesquisarCartaoCreditoPorId(compra.getCartaoCredito().getId());

    Byte diaVencimentoFatura = cartaoCreditoOp.orElseThrow().getDiaVencimentoFatura();
    Integer diaDaCompra = DateUtils.getDia(compra.getData());

    Calendar calendar = Calendar.getInstance();
    if (diaDaCompra > diaVencimentoFatura) {
      calendar.setTime(compra.getData());
      calendar.add(Calendar.MONTH, 1);
    } else {
      calendar.setTime(compra.getData());
    }

    List<Lancamento> lancamentos = new ArrayList<>();
    for (int i = 1; i <= numParcelas; i++) {
      Lancamento lancamento = new Lancamento();
      lancamento.setCompra(compra);
      lancamento.setNumParcela(i);
      lancamento.setData(calendar.getTime());
      lancamento.setValor(valorParcela);

      lancamento = lancamentoRepository.save(lancamento);
      lancamentos.add(lancamento);

      // A data da próxima parcela será 1 mês adiante.
      calendar.add(Calendar.MONTH, 1);
    }

    return lancamentos;
  }

  /**
   * Calcula a soma dos valores dos lançamentos.
   */
  public double calcularValorTotal(List<Lancamento> lancamentos) {
    double total = 0.0;

    // Itera sobre os lançamentos, acumulando o valor total.
    for (Lancamento lancamento : lancamentos) {
      if (Tipo.DEBITO == lancamento.getCompra().getTipo()) {
        total += lancamento.getValor();
      } else if (Tipo.CREDITO == lancamento.getCompra().getTipo()) {
        total -= lancamento.getValor();
      }
    }

    return total;
  }

  /**
   * Obtêm a lista de lançamentos abertos (sem fatura) para o cartão de crédito.
   * 
   * @param cartaoCreditoId Id do cartão de crédique que deseja saber os
   *                        lançamentos.
   */
  public List<Lancamento> pesquisarLancamentos(Integer cartaoCreditoId) {
    return pesquisarLancamentos(null, null, cartaoCreditoId);
  }

  /**
   * Obtém a lista de lançamentos abertos (sem fatura) para o cartão até um
   * mês/ano.
   */
  public List<Lancamento> pesquisarLancamentos(Integer mes, Integer ano, Integer cartaoCreditoId) {
    return lancamentoRepository.pesquisarLancamentos(mes, ano, cartaoCreditoId);
  }

  /**
   * Obtêm a lista de lançamentos associados a uma fatura.
   */
  public List<Lancamento> listarLancamentosFatura(Integer faturaId) {
    return lancamentoRepository.listarLancamentosFatura(faturaId);
  }

  public BigDecimal valorTotalDosLancamentosAbertos(CartaoCredito cartaoCredito) {
    double totalLancamentosAbertos = lancamentoRepository.somarLancamentosAbertos(cartaoCredito.getId());
    return new BigDecimal(totalLancamentosAbertos);
  }
}
