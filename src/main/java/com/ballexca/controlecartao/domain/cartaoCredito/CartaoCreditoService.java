package com.ballexca.controlecartao.domain.cartaoCredito;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito.Bandeira;
import com.ballexca.controlecartao.domain.exception.CartaoExistenteException;
import com.ballexca.controlecartao.domain.exception.CartaoVencidoException;
import com.ballexca.controlecartao.domain.exception.NegocioException;

public interface CartaoCreditoService {

	/**
	 * Efetua a pesquisa do cartao de crédito na base de dados pelo ID.
	 * 
	 * @param cartaoCreditoId não deve ser nulo.
	 * 
	 * @return a entidade com o id informado ou Optional#empty() se nenhum for encontrado.
	 * */
	 Optional<CartaoCredito> pesquisarCartaoCreditoPorId(Integer cartaoCreditoId);

	/**
	 * Retorna todas as instancias de CartaoCredito.
	 * 
	 * @return todas instancias.
	 * */
	List<CartaoCredito> pesquisarTodosCartoesCredito();

	/**
	 * Retorna todas as bandeiras disponíveis para o cartão de crédito.
	 * 
	 * @return todas as bandeiras disponíveis.
	 * */
	List<Bandeira> pesquisarBandeiras();

	/**
	 * Efetua o cadastro do cartao de cŕedito na base de dados.
	 * 
	 * @param cartaoCredito que deseja cadastrar.
	 * 
	 * @throws CartaoExistenteException Se já existir um cartão de crédito cadastrado com o número informado.
	 * @throws CartaoVencidoException Se a data de vencimento for anterior a data atual.
	 * */
	void salvar(CartaoCredito cartaoCredito);

	/**
	 * Efetua a atualização do cartão de crédito na base de dados.
	 * 
	 * @param cartaoCredito que deseja cadastrar.
	 * 
	 * @throws CartaoVencidoException Se a data de vencimento for anterior a data atual.
	 * @throws NegocioException Se o número do cartão for alterado. 
	 * */
	void atualizar(CartaoCredito cartaoCredito);

	/**
	 * Exclui um cartão de crédito cadastrado.
	 * 
	 * @param Id do cartão que deseja excluír.
	 * 
	 * @throws NegocioException se existirem compras ativas no cartão.
	 */
	void excluir(Integer cartaoCreditoId);
	
	/**
	 * Retorna o limite disponivel para o cartão de crédito informado.
	 * 
	 * @param Id do cartão que deseja excluír.
	 * 
	 * @throws NegocioException se existirem compras ativas no cartão.
	 */
	BigDecimal consultarLimiteDisponivel(CartaoCredito cartaoCredito);

}