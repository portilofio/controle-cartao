package com.ballexca.controlecartao.domain.cartaoCredito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import com.ballexca.controlecartao.domain.exception.CartaoExistenteException;
import com.ballexca.controlecartao.domain.exception.CartaoVencidoException;
import com.ballexca.controlecartao.domain.exception.NegocioException;
import com.ballexca.controlecartao.domain.ports.CartaoCreditoRepository;
import com.ballexca.controlecartao.domain.util.DateUtils;

public class CartaoCredito implements Serializable {
	
	public enum Bandeira {
		MASTERCARD("Mastercard"), 
		VISA("Visa"), 
		ELO("Elo"), 
		HIPERCARD("Hipercard"),
		AMERICAN_EXPRESS("American Express"), 
		ALELO("Alelo");
		
		private String label;
		
		private Bandeira(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return this.label;
		}
	}
	
	private Integer id;
	private Bandeira bandeira;
	private String descricao;
	private String numero;
	private BigDecimal limite;
	private Date dataVencimento; // TODO refatorar para LocalDate ?
	private Byte diaVencimentoFatura;

	public CartaoCredito validaNumeroExistente(CartaoCreditoRepository cartaoRepository) {
		long quantidadeExistente = cartaoRepository.countByNumero(getNumero());
		if (quantidadeExistente > 0) {
			throw new CartaoExistenteException();
		}

		return this;
	}
	
	public CartaoCredito validaAlteracaoNumero(CartaoCreditoRepository cartaoRepository) {
		CartaoCredito cartaoCadastrado = cartaoRepository.findById(getId()).orElseThrow();
		boolean igual = cartaoCadastrado.getNumero().equals(getNumero());
		if (!igual) {
			throw new NegocioException(NegocioException.MSG_NUMERO_CARTAO_ALTERADO);
		}

		return this;
	}
	
	public CartaoCredito validaCartaoVencido(CartaoCreditoRepository cartaoRepository) {
		boolean anteriorDataAtualDoSistema = DateUtils.anteriorDataAtualDoSistema(getDataVencimento());
		if (anteriorDataAtualDoSistema) {
			throw new CartaoVencidoException();
		}

		return this;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getLimite() {
		return limite;
	}

	public void setLimite(BigDecimal limite) {
		this.limite = limite;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Byte getDiaVencimentoFatura() {
		return diaVencimentoFatura;
	}

	public void setDiaVencimentoFatura(Byte diaVencimentoFatura) {
		this.diaVencimentoFatura = diaVencimentoFatura;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartaoCredito other = (CartaoCredito) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CartaoCredito [getId()=");
		builder.append(getId());
		builder.append(", getBandeira()=");
		builder.append(getBandeira());
		builder.append(", getNumero()=");
		builder.append(getNumero());
		builder.append(", getLimite()=");
		builder.append(getLimite());
		builder.append(", getDataVencimento()=");
		builder.append(getDataVencimento());
		builder.append(", getDescricao()=");
		builder.append(getDescricao());
		builder.append(", getDiaVencimentoFatura()=");
		builder.append(getDiaVencimentoFatura());
		builder.append("]");
		return builder.toString();
	}
}
