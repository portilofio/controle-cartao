package com.ballexca.controlecartao.domain.cartaoCredito;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.ballexca.controlecartao.domain.Service;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito.Bandeira;
import com.ballexca.controlecartao.domain.lancamento.LancamentoService;
import com.ballexca.controlecartao.domain.ports.CartaoCreditoRepository;

class CartaoCreditoServiceImpl extends Service implements CartaoCreditoService {
  
  private LancamentoService lancamentoService;
  private CartaoCreditoRepository cartaoCreditoRepository;

  @Inject
  public CartaoCreditoServiceImpl(CartaoCreditoRepository cartaoCreditoRepository,
      LancamentoService lancamentoService) {
    
    this.cartaoCreditoRepository = cartaoCreditoRepository;
    this.lancamentoService = lancamentoService;
  }

  @Override
  public Optional<CartaoCredito> pesquisarCartaoCreditoPorId(Integer cartaoCreditoId) {
    return cartaoCreditoRepository.findById(cartaoCreditoId);
  }

  @Override
  public List<CartaoCredito> pesquisarTodosCartoesCredito() {
    return cartaoCreditoRepository.findAll();
  }

  @Override
  public List<Bandeira> pesquisarBandeiras() {
    return Arrays.asList(CartaoCredito.Bandeira.values());
  }

  @Override
  public void salvar(CartaoCredito cartaoCredito) {
    try {
      beginTransaction();

      cartaoCredito.validaNumeroExistente(cartaoCreditoRepository).validaCartaoVencido(cartaoCreditoRepository);

      cartaoCreditoRepository.save(cartaoCredito);

      commitTransaction();
    } catch (RuntimeException e) {
      rollbackTransaction();
      throw e;
    }
  }

  @Override
  public void atualizar(CartaoCredito cartaoCredito) {
    try {
      beginTransaction();

      cartaoCredito.validaAlteracaoNumero(cartaoCreditoRepository).validaCartaoVencido(cartaoCreditoRepository);

      cartaoCreditoRepository.update(cartaoCredito);

      commitTransaction();
    } catch (RuntimeException e) {
      rollbackTransaction();
      throw e;
    }
  }

  @Override
  public void excluir(Integer cartaoCreditoId) {
    try {
      beginTransaction();

      // TODO nao deixar excluir se exisitir compra ativa.
      cartaoCreditoRepository.deleteById(cartaoCreditoId);

      commitTransaction();
    } catch (RuntimeException e) {
      rollbackTransaction();
      throw e;
    }
  }

  @Override
  public BigDecimal consultarLimiteDisponivel(CartaoCredito cartaoCredito) {
    BigDecimal valorTotalDosLancamentosAbertos = 
        lancamentoService.valorTotalDosLancamentosAbertos(cartaoCredito);

    // Obtêm os dados do cartão de crédito.
    cartaoCredito = pesquisarCartaoCreditoPorId(cartaoCredito.getId()).orElseThrow();

    BigDecimal limite = cartaoCredito.getLimite();

    // Subtrai o total gasto do limite do cartão de crédito.
    return limite.subtract(valorTotalDosLancamentosAbertos);
  }
}
