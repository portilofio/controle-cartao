package com.ballexca.controlecartao.adapters.db;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@RequestScoped
public abstract class CrudJPA implements Serializable {
	
	@PersistenceContext(name = "controleCartao")
	protected EntityManager em;

	public <T> T carregar(Object key, Class<T> clazz) {
		return em.find(clazz, key);
	}

	public <T> void salvar(T entity) {
		em.persist(entity);
	}
	
	public <T> T alterar(T entity) {
		return em.merge(entity);
	}
	
	public <T> void excluir(T entity) {
		em.remove(entity);
	}
	
	public Query criarQuery(String query) {
		return em.createQuery(query);
	}
}
