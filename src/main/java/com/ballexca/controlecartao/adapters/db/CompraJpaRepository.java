package com.ballexca.controlecartao.adapters.db;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.ballexca.controlecartao.domain.compra.Compra;
import com.ballexca.controlecartao.domain.ports.CompraRepository;

public class CompraJpaRepository extends CrudJPA implements CompraRepository {

	@Override
	public List<Compra> findAll() {
		String selectAll = "SELECT c FROM Compra c";
		TypedQuery<Compra> typedQuery = this.em.createNamedQuery(selectAll, Compra.class);
		
		return typedQuery.getResultList();
	}

	@Override
	public Optional<Compra> findById(Integer id) {
		Compra carregar = this.carregar(id, Compra.class);
		return Optional.ofNullable(carregar);
	}

	@Override
	public Compra save(Compra compra) {
		this.salvar(compra);
		return compra;
	}

	@Override
	public Compra update(Compra compra) {
		return this.alterar(compra);
	}

	@Override
	public void delete(Compra compra) {
		this.excluir(compra);		
	}

	@Override
	public void deleteById(Integer primaryKey) {
		Optional<Compra> optional = this.findById(primaryKey);		
		this.excluir(optional.orElseThrow());
	}
}
