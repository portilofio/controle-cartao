package com.ballexca.controlecartao.adapters.db;

import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;
import com.ballexca.controlecartao.domain.ports.CartaoCreditoRepository;

public class CartaoCreditoJpaRepository extends CrudJPA implements CartaoCreditoRepository {

	@Override
	public List<CartaoCredito> findAll() {
		return listarCartoesCredito();
	}

	@Override
	public Optional<CartaoCredito> findById(Integer id) {
		CartaoCredito cartaoCredito = carregar(id, CartaoCredito.class);
		return Optional.ofNullable(cartaoCredito);
	}

	@Override
	public CartaoCredito save(CartaoCredito cartaoCredito) {
		salvar(cartaoCredito);
		return cartaoCredito;
	}

	@Override
	public CartaoCredito update(CartaoCredito cartaoCredito) {
		return alterar(cartaoCredito);
	}

	@Override
	public void delete(CartaoCredito cartaoCredito) {
		excluir(cartaoCredito);
	}

	@Override
	public void deleteById(Integer id) {
		Optional<CartaoCredito> optional = findById(id);
		excluir(optional.orElseThrow());
	}

	@Override
	public Optional<CartaoCredito> findByNumero(String numero) {
		String select = String.format("SELECT c FROM CartaoCredito c WHERE c.numero = '%s'", numero);
		TypedQuery<CartaoCredito> typedQuery = em.createQuery(select, CartaoCredito.class);
		
		try {
			CartaoCredito singleResult = typedQuery.getSingleResult();
			return Optional.of(singleResult);
		}catch (NoResultException e) {
			return Optional.empty();
		}
	}

	@Override
	public Optional<CartaoCredito> findByNumeroAndId(String numero, Integer id) {
		String select = String.format("SELECT c FROM CartaoCredito c WHERE c.numero = '%s' AND c.id = %d", numero, id);
		TypedQuery<CartaoCredito> typedQuery = em.createQuery(select, CartaoCredito.class);
		
		try {
			CartaoCredito singleResult = typedQuery.getSingleResult();
			return Optional.of(singleResult);
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	@Override
	public long countByNumero(String numero) {
		String selectCount = String.format("SELECT COUNT(c) FROM CartaoCredito C WHERE c.numero = '%s'", numero);
		Query query = criarQuery(selectCount);
		
		return (Long) query.getSingleResult();
	}

	@Override
	public long countById(Integer id) {
		String selectCount = String.format("SELECT COUNT(c) FROM CartaoCredito C WHERE c.id = %d", id);
		Query q = criarQuery(selectCount);
		
		return (Long) q.getResultList().get(0);
	}

	private List<CartaoCredito> listarCartoesCredito() {
		String select = "SELECT c FROM CartaoCredito c ORDER BY c.bandeira, c.dataVencimento";
		TypedQuery<CartaoCredito> typedQuery = em.createQuery(select, CartaoCredito.class);
		
		return typedQuery.getResultList();
	}
}
