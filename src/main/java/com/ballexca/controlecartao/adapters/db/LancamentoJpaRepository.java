package com.ballexca.controlecartao.adapters.db;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.ballexca.controlecartao.domain.lancamento.Lancamento;
import com.ballexca.controlecartao.domain.ports.LancamentoRepository;
import com.ballexca.controlecartao.domain.util.DateUtils;

public class LancamentoJpaRepository extends CrudJPA implements LancamentoRepository {
	/**
	 * Pesquisa lançamentos abertos (sem fatura) para um cartão até o mês/ano
	 * fornecido. Se mês/ano forem null, lista todos os lançamentos abertos, sem
	 * filtrar pela data.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Lancamento> pesquisarLancamentos(Integer mes, Integer ano, Integer cartaoCreditoId) {

		String query = "SELECT l FROM Lancamento l WHERE l.fatura IS NULL AND l.compra.cartaoCredito.id = "
				+ cartaoCreditoId;
		// TODO Criar o select com string format?
		if (mes != null && ano != null) {
			query += " AND l.data <= :data";
		}

		query += " ORDER BY l.data";

		Query q = criarQuery(query);

		if (mes != null && ano != null) {
			// Se um mês/ano for fornecido, cria um Calendar para comparação de datas.
			Calendar calendar = Calendar.getInstance();
			calendar.set(ano, mes, DateUtils.getMaxDays(mes, ano));
			q.setParameter("data", calendar, TemporalType.DATE);
		}

		return q.getResultList();
	}

	/**
	 * Obtêm a lista de lançamentos de uma compra.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Lancamento> listarLancamentosCompra(Integer compraId) {
		String select = "SELECT l FROM Lancamento l WHERE l.compra.id = " + compraId;
		// TODO Criar o select com string format?
		Query q = criarQuery(select);

		return q.getResultList();
	}

	/**
	 * Obtêm a lista de lançamentos de uma fatura.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Lancamento> listarLancamentosFatura(Integer faturaId) {
		// TODO Criar o select com string format?
		Query q = criarQuery("SELECT l FROM Lancamento l WHERE l.fatura.id = " + faturaId);
		return q.getResultList();
	}

	/**
	 * Soma os valores dos lançamentos abertos de um cartão de crédito.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public double somarLancamentosAbertos(Integer cartaoCreditoId) {
		// TODO Criar o select com string format?
		Query q = criarQuery("SELECT SUM(l.valor) FROM Lancamento l WHERE l.compra.cartaoCredito.id = "
				+ cartaoCreditoId + " AND l.fatura IS NULL");
		List<Double> results = q.getResultList();

		if (results.size() == 0) {
			return 0;
		}

		return results.get(0);
	}

	@Override
	public List<Lancamento> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Lancamento> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Lancamento save(Lancamento lancamento) {
		this.salvar(lancamento);
		return lancamento;
	}

	@Override
	public Lancamento update(Lancamento lancamento) {
		return this.alterar(lancamento);
	}

	@Override
	public void delete(Lancamento lancamento) {
		this.delete(lancamento);
	}

	@Override
	public void deleteById(Integer lancamentoId) {
		Optional<Lancamento> optional = findById(lancamentoId);
		this.delete(optional.orElseThrow());
	}

	@Override
	public Optional<Lancamento> findByCompraIdAndNumeroParcela(Integer compraId, Integer numeroParcela) {
		String select = 
				String.format("SELECT l FROM Lancamento l WHERE l.compra.id = %d AND l.numParcela = %d", compraId, numeroParcela);
		
		TypedQuery<Lancamento> typedQuery = this.em.createNamedQuery(select, Lancamento.class);
		List<Lancamento> lancamentos = typedQuery.getResultList();

		if (lancamentos.size() == 0) {
			return Optional.empty();
		}

		return Optional.of(lancamentos.get(0));
	}
}
