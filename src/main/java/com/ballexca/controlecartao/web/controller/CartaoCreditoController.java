package com.ballexca.controlecartao.web.controller;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito.Bandeira;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCreditoService;


@RequestScoped
@Named("cartaoBean")
public class CartaoCreditoController extends AbstractController {
	
	@Inject
	private CartaoCreditoService cartaoCreditoService;

	private CartaoCredito cartaoCredito;
	
	private List<CartaoCredito> cartoesCredito;
	
	private boolean renderizaTabelaCartoes = false;
	private boolean modoAtualizacao = false;
	
	@PostConstruct
	public void init() {
		try {
			getCartoesCredito();
			
			if(cartoesCredito.isEmpty() == false) {
				renderizaTabelaCartoes = true;
			}
		} catch (Exception e) {
			addMessageToRequest(TipoMensagem.ERROR, "Erro ao carregar os dados de cartões de crédito.");
		}
	}
		
	public String gravar() throws Exception {
		try {
			if (cartaoCredito.getId() == null) {
				cartaoCreditoService.salvar(cartaoCredito);
			} else {
				cartaoCreditoService.atualizar(cartaoCredito);
			}
			
			cartaoCredito = null;
			addMessageToRequest(TipoMensagem.INFO, "Registro cadastrado com sucesso!");
			return redirect("cadastrar_cartao");
		} catch (Exception e) {
			addMessageToRequest(TipoMensagem.ERROR, e.getLocalizedMessage());
			return this.mesmaPagina;
		}
	}

	public String carregarCartao(Integer cartaoCreditoId) {
		try {
			Optional<CartaoCredito> optional = cartaoCreditoService.pesquisarCartaoCreditoPorId(cartaoCreditoId);
			cartaoCredito = optional.orElseThrow();
			modoAtualizacao = true;
			
			return this.mesmaPagina;
		} catch (Exception e) {
			addMessageToRequest(TipoMensagem.ERROR, "Não foi possível carregar o cartão de crédito.");
			return this.mesmaPagina;
		}
	}
	
	public String excluir(Integer cartaoCreditoId) throws Exception {
		cartaoCreditoService.excluir(cartaoCreditoId);
		cartoesCredito = null;
		addMessageToRequest(TipoMensagem.INFO, "Registro excluído com sucesso!");
		return redirect("cadastrar_cartao");
	}
	
	public String limpar() throws Exception {
		cartaoCredito = null;
		return this.mesmaPagina;
	}

	public CartaoCredito getCartaoCredito() {
		if (cartaoCredito == null) {
			cartaoCredito = new CartaoCredito();
		}
		return cartaoCredito;
	}

	public List<CartaoCredito> getCartoesCredito() throws Exception {
		if (cartoesCredito == null) {
			cartoesCredito = cartaoCreditoService.pesquisarTodosCartoesCredito();
		}
		return cartoesCredito;
	}

	public List<Bandeira> getBandeiras() {
		return cartaoCreditoService.pesquisarBandeiras();
	}
	
	public boolean isRenderizaTabelaCartoes() {
		return renderizaTabelaCartoes;
	}
	
	public boolean isModoAtualizacao() {
		return modoAtualizacao;
	}
}
