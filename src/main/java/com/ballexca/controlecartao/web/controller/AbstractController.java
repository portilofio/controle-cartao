package com.ballexca.controlecartao.web.controller;

import java.io.Serializable;

import javax.faces.context.Flash;
import javax.inject.Inject;

/**
 * Superclasse de todos os Controller JSF.
 */
public class AbstractController implements Serializable {
	
	public enum TipoMensagem {
		INFO, ERROR
	}
	
	@Inject
	private Flash flash;
	
	protected String mesmaPagina = null;
	
	/**
	 * Adiciona uma mensagem ao escopo flash(Map, ou seja, armazena dados no formato => chave:valor).
	 * Esta mensagem pode ser exibida na tela através da chamada <strong>#{flash.msg}</strong>
	 * 
	 * @param TipoMensagem determina o tipo de mensagem que será exibida. Dependendo do tipo a mensagem terá
	 * uma cor diferente.
	 * 
	 * @param String mensagem que será exibida.
	 */
	protected void addMessageToRequest(TipoMensagem tipoMensagem, String mensagem) {
		StringBuilder builder = new StringBuilder();
		builder.append(tipoMensagem.toString());
		builder.append(":");
		builder.append(mensagem);
		
		flash.put("msg", builder.toString());
	}
	
	/**
	 * Configura um redirect para o outcome especificado.
	 * 
	 * @param String O outcome para onde deseja fazer o redirect.
	 * @return String outcome sufixado com <strong>?faces-redirect=true</strong>
	 */
	protected String redirect(String outcome) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(outcome);
		builder.append("?faces-redirect=true");
		
		return builder.toString();
	}
}
