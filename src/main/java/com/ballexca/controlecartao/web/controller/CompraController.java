package com.ballexca.controlecartao.web.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCreditoService;
import com.ballexca.controlecartao.domain.compra.Compra;
import com.ballexca.controlecartao.domain.compra.CompraServiceImpl;
import com.ballexca.controlecartao.domain.compra.Compra.Tipo;
import com.ballexca.controlecartao.domain.exception.NegocioException;

@RequestScoped
@Named("compraController")
public class CompraController extends AbstractController {

	@Inject
	private CompraServiceImpl compraService;
	
	@Inject 
	private CartaoCreditoService cartaoCreditoService;
	
	private Compra compra;
	private List<CartaoCredito> cartoesCredito;
	
	@PostConstruct
	public void init() {
		compra = new Compra();
		compra.setCartaoCredito(new CartaoCredito());
		compra.setNumParcelas((byte) 1);
		compra.setTipo(Tipo.DEBITO);
	}
	
	public String gravar() {
		try {
			compraService.salvar(compra);
			compra = null;
		
			addMessageToRequest(TipoMensagem.INFO, "Registro cadastrado com sucesso!");
			return redirect("cadastrar_compra");
		} catch(NegocioException e) {
			addMessageToRequest(TipoMensagem.ERROR, e.getLocalizedMessage());
			return this.mesmaPagina;
		}
	}
	
	public String limpar() {
		compra = new Compra();
		return this.mesmaPagina;
	}
	
	public List<CartaoCredito> getCartoesCredito() throws Exception {
		if (cartoesCredito == null) {
			cartoesCredito = cartaoCreditoService.pesquisarTodosCartoesCredito();
		}
		
		return cartoesCredito;
	}
	
	public Tipo[] getTipos() {
		return Tipo.values();
	}

	public Compra getCompra() {
		if(compra == null) {
			return new Compra();
		}
		
		return compra;
	}
}
