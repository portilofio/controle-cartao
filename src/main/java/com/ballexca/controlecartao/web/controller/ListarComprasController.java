package com.ballexca.controlecartao.web.controller;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCredito;
import com.ballexca.controlecartao.domain.cartaoCredito.CartaoCreditoService;
import com.ballexca.controlecartao.domain.compra.Compra;
import com.ballexca.controlecartao.domain.compra.CompraServiceImpl;
import com.ballexca.controlecartao.domain.lancamento.Lancamento;
import com.ballexca.controlecartao.domain.lancamento.LancamentoService;
import com.ballexca.controlecartao.domain.util.DateUtils;

@SessionScoped
@Named("listarComprasBean")
public class ListarComprasController extends AbstractController {
	
	@Inject
	private CompraServiceImpl compraService;
	
	@Inject
	private CartaoCreditoService cartaoCreditoService;
	
	@Inject
	private LancamentoService lancamentoService;
	
	private Date mesAno;
	
	private CartaoCredito cartaoCredito;
	
	private List<Lancamento> lancamentos;
	
	private List<CartaoCredito> cartoesCredito;
	
	private Double valorTotal;
	
	private Double valorDisponivel;
	
	/**
	 * Flag para controlar se os atributos do bean devem ser resetados. O valor desta flag eh obtido atraves de um 
	 * parametro passado na request ao clicar no link que abre a pagina
	 */
	private boolean limpar;
	
	/**
	 * Reseta os atributos do bean. Este metodo eh chamado toda vez que a view eh renderizada
	 * (evento preRenderView)
	 */
	public void init(ComponentSystemEvent event) {
		if (limpar) {
			lancamentos = null;
			mesAno = null;
			cartaoCredito = null;
			cartoesCredito = null;
			valorTotal = null;
			valorDisponivel = null;
			limpar = false;
		}
	}
	
	/**
	 * Obtêm a lista de lançamentos abertos para um cartão até determinado mês/ano.
	 */
	public String pesquisar() throws Exception {
		lancamentos = lancamentoService.pesquisarLancamentos(DateUtils.getMes(mesAno), DateUtils.getAno(mesAno), cartaoCredito.getId());
		
		valorTotal = lancamentoService.calcularValorTotal(lancamentos);
		
		valorDisponivel = cartaoCreditoService.consultarLimiteDisponivel(cartaoCredito).doubleValue();
		
		cartaoCredito = cartaoCreditoService.pesquisarCartaoCreditoPorId(cartaoCredito.getId()).orElseThrow();
		
		return null;
	}
	
	/**
	 * Obtêm a lista de cartões cadastrados.
	 */
	public List<CartaoCredito> getCartoesCredito() throws Exception {
		if (cartoesCredito == null) {
			cartoesCredito = cartaoCreditoService.pesquisarTodosCartoesCredito();
		}
		
		return cartoesCredito;
	}
	
	/**
	 * Exclui uma compra e todos os lançamentos atrelados a ela.
	 */
	public String excluir(Integer compraId) throws Exception {
		Compra compra = new Compra();
		compra.setId(compraId);
		
		compraService.excluirCompra(compra);
		
		pesquisar();
		
		return redirect("listar_compras");
	}

	public List<Lancamento> getLancamentos() throws Exception {
		return lancamentos;
	}

	public Date getMesAno() {
		return mesAno;
	}

	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public CartaoCredito getCartaoCredito() {
		if (cartaoCredito == null) {
			 cartaoCredito = new CartaoCredito();
		}
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public boolean isLimpar() {
		return limpar;
	}

	public void setLimpar(boolean limpar) {
		this.limpar = limpar;
	}

	public Double getValorDisponivel() {
		return valorDisponivel;
	}
}
