package com.ballexca.controlecartao.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Conversor customizado para números de cartão de credito
 */
@FacesConverter("converter.NumeroCartaoCredito")
public class NumeroCartaoCreditoConverter implements Converter<String> {

	@Override
	public String getAsObject(FacesContext context, UIComponent component, String value) {
		return null;
	}

	/**
	 * Converte o número do cartão para uma representação onde apenas os 4 primeiros 
	 * dígitos são exibidos.
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, String value) {
		String numero = (String) value;
		
		String inicio = numero.substring(0, 4);
		String secreto = "****";
		
		return inicio + " " + secreto + " " + secreto + " " + secreto;
	}
}
