package com.ballexca.controlecartao.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.ballexca.controlecartao.domain.util.StringUtils;

@FacesValidator("validator.NumeroCartaoCredito")
public class NumeroCartaoCreditoValidator implements Validator<String> {
	private byte quantidadeDeDigitosDoCartao = 16;
	
	@Override
	public void validate(FacesContext context, UIComponent component, String numero) 
			throws ValidatorException {
		
		if (numero == null) {
			return;
		}
		
		numero = numero.trim();
		
		if (!StringUtils.isNumeric(numero)) {
			FacesMessage msg = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"O valor digitado não é um número válido.",
					null);
			
			throw new ValidatorException(msg);
		}
				
		if (numero.length() != quantidadeDeDigitosDoCartao) {
			FacesMessage msg = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"O número do cartão de crédito deve possuir 16 dígitos numéricos.",
					null);
			
			throw new ValidatorException(msg);
		}
	}
}
