package com.ballexca.controlecartao.domain.cartaoCredito;

import static com.ballexca.controlecartao.domain.cartaoCredito.CartaoCreditoBuilder.umCartaoCredito;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.mockito.Mockito;

import com.ballexca.controlecartao.domain.lancamento.LancamentoService;
import com.ballexca.controlecartao.domain.ports.CartaoCreditoRepository;

public class CartaoCreditoServiceUnitTest {

  @Rule
  public ErrorCollector error = new ErrorCollector();

  private CartaoCreditoService cartaoCreditoService;
  private LancamentoService lancamentoService;
  private CartaoCreditoRepository cartaoCreditoRepository;

  @Before
  public void init() {
    this.cartaoCreditoRepository = Mockito.mock(CartaoCreditoRepository.class);
    this.lancamentoService = Mockito.mock(LancamentoService.class);

    this.cartaoCreditoService = new CartaoCreditoServiceImpl(this.cartaoCreditoRepository, this.lancamentoService);
  }

  @Test
  public void testeJunit() {
    Assert.assertTrue(true);
    Assert.assertFalse(false);
  }

  @Test
  public void quandoCadastrarCartao_seForValido_insiraNaBaseDeDados() {
    CartaoCredito cartaoCredito = umCartaoCredito().comNumero("").agora();

    cartaoCreditoService.salvar(cartaoCredito);
  }
}
