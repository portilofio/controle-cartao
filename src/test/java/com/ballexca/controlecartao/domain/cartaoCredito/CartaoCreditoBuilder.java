package com.ballexca.controlecartao.domain.cartaoCredito;

public class CartaoCreditoBuilder {

  private CartaoCredito cartaoCredito = new CartaoCredito();
  
  public static CartaoCreditoBuilder umCartaoCredito() {
    return new CartaoCreditoBuilder();
  }

  private CartaoCreditoBuilder() {
  }

  public CartaoCreditoBuilder comNumero(String numero) {
    this.cartaoCredito.setNumero(numero);
    return this;
  }

  public CartaoCredito agora() {
    return this.cartaoCredito;
  }
}
